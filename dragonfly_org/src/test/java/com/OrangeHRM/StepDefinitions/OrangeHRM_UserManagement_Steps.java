package com.OrangeHRM.StepDefinitions;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;


import com.OrangeHRM.pages.AdminPage;
import com.OrangeHRM.pages.DashboardPage;

import com.OrangeHRM.pages.LoginPage;
import com.OrangeHRM.pages.UserDetailsPage;
import com.OrangeHRM.pages.UserPage;
import com.OrangeHRM.util.BasePage;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;

public class OrangeHRM_UserManagement_Steps extends BasePage{
	
    LoginPage loginPage;
    DashboardPage dashboardPage;
    AdminPage adminPage;
    UserPage userPage;
    UserDetailsPage userDetailsPage;
    
   
    @Before("@Second")
    public void launchUrl()
    {
    	System.out.println("Before Method before initialization in user steps");
    	BasePage.initialization();
    	loginPage=new LoginPage();
		dashboardPage=loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
    }
    
    
	@Given("user logged in as admin")
	public void user_logged_in_as_admin() {
		Assert.assertEquals("Dashboard", dashboardPage.verifyDashboardPage());
	}
	@When("user click on admin and hover usermanagement and click on users")
	public void hover_admin_and_usermanagement_and_click_on_users() throws InterruptedException {
		adminPage=dashboardPage.clickOnAdmin();
		userPage=adminPage.clickOnUsers();
	}
	@Then("click on Add user")
	public void click_on_Add_user() {
	    userDetailsPage=userPage.clickOnAdd();
	}
	@Then("enter userRole, employeeName, username and status and click on Save button")
	public void enter_new_user_details() throws InterruptedException
	{
		userDetailsPage.enter_new_user_details(userProp.getProperty("userRole"), userProp.getProperty("employeeName"), userProp.getProperty("userName"),userProp.getProperty("status"),userProp.getProperty("password"));
	}
	@Then("User should be added in the users list")
	public void user_should_be_added_in_the_users_list() {
		Assert.assertTrue(userPage.verifyUser(userProp.getProperty("userName")));
	}


	
    @Given("admin should be logged in and go to users page")
    public void admin_should_be_logged_in_and_go_to_users_page() throws InterruptedException {
    	adminPage=dashboardPage.clickOnAdmin();
		userPage=adminPage.clickOnUsers();
    }

    @When("admin selects user to be edited")
    public void admin_selects_user_to_be_edited() {
       userDetailsPage=userPage.editUser(userProp.getProperty("userTobeEdited"));
    }

    @Then("all the fields should be disabled")
    public void all_the_fields_should_be_disabled() {
       Assert.assertTrue(userDetailsPage.verifyAllFiledsAreDisabled());
    }

    @Then("click on Edit button and edit the fields and click on Save button")
    public void click_on_Edit_button_and_edit_the_fields_and_click_on_Save_button() throws InterruptedException {
       userDetailsPage.enter_edit_user_details(userProp.getProperty("userRoleEdit"), userProp.getProperty("empNameEdit"), userProp.getProperty("userNameEdit"), userProp.getProperty("statusEdit"));
    }

    @Then("all the changes must be reflected when you click on the user again")
    public void all_the_changes_must_be_reflected_when_you_click_on_the_user_again() {
    	Assert.assertTrue(userPage.verifyEditUser(userProp.getProperty("userNameEdit")));
    }

	
	
	
	
	@Given("admin is already logged in and navigated to users page")
	public void admin_is_already_logged_in_and_navigated_to_users_page() throws InterruptedException {
		adminPage=dashboardPage.clickOnAdmin();
		userPage=adminPage.clickOnUsers();
	}

	@When("user clicks on delete button")
	public void user_clicks_on_delete_button() {
	   userPage.clickOnDelete();
	}

	@Then("nothing should happen delete button must be disabled")
	public void nothing_should_happen_delete_button_must_be_disabled() {
	    Assert.assertFalse(userPage.verifyDeleteBtnIsDisabled());
	}
	
	
	
	@Given("admin logs in and go to admin page")
	public void admin_logs_in_and_go_to_admin_page() throws InterruptedException {
	   System.out.println("############ Delete User Scenario ############");
	   Assert.assertEquals("Dashboard", dashboardPage.verifyDashboardPage());
	   adminPage=dashboardPage.clickOnAdmin();
	}
	@When("admin selects user to be deleted from the list of users")
	public void admin_selects_user_to_be_deleted_from_the_list_of_users() throws InterruptedException {
		userPage=adminPage.clickOnUsers();
	    userPage.selectUser(userProp.getProperty("userTobeDeleted"));
	}

	@Then("delete button should be enabled and click on delete button")
	public void delete_button_should_be_enabled_and_click_on_delete_button() {
	    userPage.deleteUser();
	}

	@Then("selected user should be deleted from the users list")
	public void selected_user_should_be_deleted_from_the_users_list() {
		Assert.assertFalse(userPage.verifyUser(userProp.getProperty("userTobeDeleted")));
	}
    
    

	@After
	public void teardown()
	{
		BasePage.tearDown();
	}

	

    
}
