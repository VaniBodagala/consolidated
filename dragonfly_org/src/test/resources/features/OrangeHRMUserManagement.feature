#Author: vani
#Feature: List of scenarios.
#Scenario: POM example
Feature: feature to verify user management

  #@SmokeTests
 #Scenario Outline: verify OrangeHRM admin add user functionality
    #Given user logged in as admin
    #When user click on admin and hover usermanagement and click on users
    #Then click on Add user
    #Then enter <userRole>, <employeeName>, <username> and <status> and click on Save button
    #Then User should be added in the users list
#
    #Examples: 
      #| userRole | employeeName | username | status  |
      #| ESS      | Fiona Grace  | abcdefgh  | Enabled |
      
      
  @Second @User 
  Scenario: verify OrangeHRM admin add user functionality
    Given user logged in as admin
    When user click on admin and hover usermanagement and click on users
    Then click on Add user
    Then enter userRole, employeeName, username and status and click on Save button
    Then User should be added in the users list


   
   @Second @User 
   Scenario: verify OrangeHRM edit user functionality
   Given admin should be logged in and go to users page
   When admin selects user to be edited 
   Then all the fields should be disabled
   Then click on Edit button and edit the fields and click on Save button
   Then all the changes must be reflected when you click on the user again 
   
    
   
   @Second @User
    Scenario: verify delete button is disabled
    Given admin is already logged in and navigated to users page
    When user clicks on delete button
    Then nothing should happen delete button must be disabled
    
        
  @Second @User
  Scenario: verify OrangeHRM delete user functionality
    Given admin logs in and go to admin page
    When admin selects user to be deleted from the list of users 
    Then delete button should be enabled and click on delete button
    Then selected user should be deleted from the users list
   
