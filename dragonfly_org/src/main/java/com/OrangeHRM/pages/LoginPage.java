package com.OrangeHRM.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.OrangeHRM.util.BasePage;

public class LoginPage extends BasePage{
	
	
	DashboardPage dashboardPage;
	private By txt_username=By.id("txtUsername");
	private By txt_password=By.id("txtPassword");
	private By btn_login=By.id("btnLogin");
	private By heading_loginPanel=By.id("logInPanelHeading");
	
	
	
	public WebElement getUsername()
	{
		return getElement(txt_username)	;
	}
	public WebElement getPassword()
	{
		return getElement(txt_password);
	}
	public WebElement getLoginBtn()
	{
		return getElement(btn_login);
	}
	public WebElement getLoginPanelHeading()
	{
		return getElement(heading_loginPanel);
	}
		
	

	public DashboardPage login(String username,String password)
	{
		getUsername().clear();
	    getUsername().sendKeys(username);
	    getPassword().clear();
		getPassword().sendKeys(password);
		getLoginBtn().click();
		return new DashboardPage();
	
	}
	
	public boolean verifyLoginPanelIsDisplayed()
	{
		return driver.findElement(heading_loginPanel).isDisplayed();
	}
}
